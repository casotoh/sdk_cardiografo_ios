//
//  CMDBluetooth.swift
//  SDK_cardiografo
//
//  Created by Camilo Soto on 5/17/19.
//  Copyright © 2019 Pammos. All rights reserved.
//

import Foundation


class CMDBluetooth: NSObject {
    
    
    class func cmdSyncTime() -> Data{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMddHHmmss"
        let dateStr = dateFormatter.string(from: Date())
        let arrayDate = Array(dateStr)
        let year = convertCharToInt(val: arrayDate[0]) * 1000 + convertCharToInt(val: arrayDate[1]) * 100 + convertCharToInt(val: arrayDate[2]) * 10 + convertCharToInt(val: arrayDate[3])
        //let yearLow = convertCharToInt(val: arrayDate[2]) * 10 + convertCharToInt(val: arrayDate[3])
        let month = convertCharToInt(val: arrayDate[4]) * 10 + convertCharToInt(val: arrayDate[5])
        let day = convertCharToInt(val: arrayDate[6]) * 10 + convertCharToInt(val: arrayDate[7])
        let hour = convertCharToInt(val: arrayDate[8]) * 10 + convertCharToInt(val: arrayDate[9])
        let minute = convertCharToInt(val: arrayDate[10]) * 10 + convertCharToInt(val: arrayDate[11])
        let seconds = convertCharToInt(val: arrayDate[12]) * 10 + convertCharToInt(val: arrayDate[13])
        var byteSend = [UInt8](repeating: 0x00, count: 64)
        byteSend[0] = 0x82
        byteSend[1] = UInt8(year >> 7 & 255)
        byteSend[2] = UInt8(year & 127)
        byteSend[3] = UInt8(month)
        byteSend[4] = UInt8(day)
        byteSend[5] = UInt8(hour)
        byteSend[6] = UInt8(minute)
        byteSend[7] = UInt8(seconds)
        
        return Data(bytes: byteSend)
    }
    
    class func cmdSetParameters(parameters: Parameters) -> Data{
        var byteSend = [UInt8](repeating: 0x00, count: 64)
        byteSend[0] = 0x83
        byteSend[1] = 0x01
        byteSend[2] = UInt8(parameters.language)
        byteSend[3] = UInt8(parameters.filter ? 1 : 0)
        byteSend[4] = UInt8(parameters.antialias ? 1 : 0)
        byteSend[5] = UInt8(parameters.measureMode ? 1 : 0)
        byteSend[6] = UInt8(parameters.timeChoose)
        byteSend[7] = UInt8(!parameters.sound ? 1 : 0)
        byteSend[8] = UInt8(parameters.shield ? 1 : 0)
        
        return Data(bytes: byteSend)
    }
    
    class func cemGetInfoData(type: Int, index: Int) -> Data{
        var byteSend = [UInt8](repeating: 0x00, count: 64)
        byteSend[0] = 0x90
        byteSend[1] = UInt8(type)
        byteSend[2] = UInt8(index & 127)
        byteSend[3] = UInt8(index >> 7 & 255)
        
        return Data(bytes: byteSend)
    }
    
    class func cmdGetData(index: Int) -> Data{
        var byteSend = [UInt8](repeating: 0x00, count: 64)
        byteSend[0] = 0xA0
        byteSend[1] = UInt8(index & 127)
        byteSend[2] = UInt8(index >> 7 & 127)
        
        return Data(bytes: byteSend)
        
    }
    
    class func cmdDeleteData() -> Data{
        var byteSend = [UInt8](repeating: 0x00, count: 64)
        byteSend[0] = 0xB0
        
        return Data(bytes: byteSend)
    }
    
    class func getStructInfo(array: [UInt8]) -> CellData{
        let a = UInt16(array[3])
        let b = UInt16(array[4])
        let mYear = Int((a << 7 | b & 255))
        let mMonth = Int(array[5] & 127)
        let mDay = Int(array[6] & 127)
        let mHour = Int(array[7] & 127)
        let mMin = Int(array[8] & 127)
        let mSec = Int(array[9] & 127)
        let hr = Int(((array[14] & 255) << 7 | array[15] & 255))
        let array10 = UInt16(array[10])
        let array11 = UInt16(array[11])
        let array12 = UInt16(array[12])
        let array13 = UInt16(array[13])
        let sizeData = Int(array10 << 21 | array11 << 14 | array12 << 7 | array13)
        
        let dateStr = "\(mDay)/\(mMonth)/\(mYear) \(mHour):\(mMin):\(mSec)"
        let idMeasure = getIdentifierData()
        let cellData = CellData(idMeasure: idMeasure, date: dateStr, valueHR: "\(hr)", type: getType(byte: array[16]), sizeData: sizeData * 2)
        setIdentifierData(id: idMeasure + 1)
        
        return cellData
    }
    
    class func convertCharToInt(val: Character)->Int{
        if let number = Int(String(val)) {
            return number
        }
        return 0
    }
    
    class func getType(byte: UInt8) -> String{
        switch byte {
        case 0x01:
            return "Missed Beat"
        case 0x02:
            return "Accidental VPB"
        case 0x03:
            return "VPB Trigeminy"
        case 0x04:
            return "VPB Bigeminy"
        case 0x05:
            return "VPB Couple"
        case 0x06:
            return "VPB Runs of 3"
        case 0x07:
            return "VPB Runs of 4"
        case 0x08:
            return "VPB RonT"
        case 0x09:
            return "Bradycardia"
        case 0x0A:
            return "Tachycardia"
        case 0x0B:
            return "Arrhythmia"
        case 0x0C:
            return "ST Elevation"
        case 0x0D:
            return "ST Depression"
        default:
            return "No abnormal"
        }
    }
    
    class func setIdentifierData(id: Int64){
        let defaults = UserDefaults.standard
        defaults.set(id, forKey: "KEY_IDENTIFIER")
    }
    
    class func getIdentifierData() -> Int64{
        if let a =  UserDefaults.standard.string(forKey: "KEY_IDENTIFIER") {
            if let idPending = Int64.init(a) {
                return idPending
            }
        }
        
        return 0
    }
    
    class func unPack(data: [UInt8]) -> [UInt8]{
        
        var dataFinal = data
        for i in 0..<8 {
            if i == 7{
                dataFinal[11 + i * 7] = dataFinal[11 + i * 7] | dataFinal[3 + i] << 7 & UInt8(128)
            }else{
                for j in 0..<7{
                    dataFinal[11 + i * 7 + j] = dataFinal[11 + i * 7 + j] | dataFinal[3 + i] << (7 - j) & UInt8(128)
                }
            }
        }
        
        
        return dataFinal
    }
}
