//
//  ChartVC.swift
//  SDK_cardiografo
//
//  Created by Camilo Soto on 5/27/19.
//  Copyright © 2019 Pammos. All rights reserved.
//

import UIKit
import Charts
import PDFKit
import SimplePDF
import MessageUI

private class CubicLineSampleFillFormatter: IFillFormatter {
    func getFillLinePosition(dataSet: ILineChartDataSet, dataProvider: LineChartDataProvider) -> CGFloat {
        return -1
    }
}

class ChartVC: UIViewController, MFMailComposeViewControllerDelegate {

    @IBOutlet weak var viewChart: LineChartView!
    @IBOutlet weak var viewPDF: UIView!
    
    var idMeasure: Int64 = 0
    var dataCell: CellData!
    var listImages: [UIImage] = []
    var type = ""
    fileprivate var fileUrl: URL?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let data = Queries.getDataForIdMeasure(idMeasure: dataCell.idMeasure)
        self.type = dataCell.type
        let imageQ = updateGraph(valueData: data, pos: 1)
        listImages.append(imageQ)
        
        for i in 1...6 {
            let image = updateGraph(valueData: data, pos: i)
            listImages.append(image)
        }
        self.listImages.remove(at: 0)
        let newImage = linkImages(arrayImages: self.listImages)
        let fileName = Utils.generatePDF(image: newImage, dataInfo: dataCell)
        viewChart.isHidden = true
        showPDF(fileName: fileName)
        
    }
    
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func send(_ sender: UIBarButtonItem) {
        //Check to see the device can send email.
        if( MFMailComposeViewController.canSendMail() ) {
            print("Can send email.")
            
            let mailComposer = MFMailComposeViewController()
            mailComposer.mailComposeDelegate = self
            
            //Set the subject and message of the email
            mailComposer.setSubject("Grafica ECG")
            mailComposer.setMessageBody("Envio del resultado de la medición", isHTML: false)
            
            if let dir = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.allDomainsMask, true).first {
                fileUrl = URL(fileURLWithPath: dir).appendingPathComponent("report_ecg.pdf")
            }
            
            if let filePath = fileUrl?.path{
                print("File path loaded.")
                
                if let fileData = NSData(contentsOfFile: filePath) {
                    print("File data loaded.")
                    mailComposer.addAttachmentData(fileData as Data, mimeType: "application/pdf", fileName: "report_ecg_\(dataCell.valueHR).pdf")
                }
            }
            self.present(mailComposer, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(
                title: "Alerta",
                message: "El usuario no tiene habilitado el envio de Email", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func updateGraph(valueData: [Float], pos: Int) -> UIImage{
        
        var linechartEntry = [ChartDataEntry]()
        
        let valInit = (pos - 1) * 400
        let valFinal = pos * 400
        
        for i in valInit..<valFinal{
            var value = ChartDataEntry(x: Double(i), y: Double(valueData[valueData.count - 1]))
            if i < valueData.count{
                value = ChartDataEntry(x: Double(i), y: Double(valueData[i]))
            }
            linechartEntry.append(value)
        }
        
        let line1 = LineChartDataSet(values: linechartEntry, label: nil)
        
        line1.colors = [NSUIColor.black]
        line1.circleHoleColor = NSUIColor.blue
        line1.circleColors = [NSUIColor.black]
        line1.circleRadius = 0
        
        let data = LineChartData()
        
        data.addDataSet(line1)
        viewChart.xAxis.gridLineDashLengths = [1, 1]
        viewChart.leftAxis.gridLineDashLengths = [1, 1]
        viewChart.xAxis.drawLabelsEnabled = false
        viewChart.xAxis.drawAxisLineEnabled = false
        viewChart.leftAxis.drawLabelsEnabled = false
        viewChart.leftAxis.drawAxisLineEnabled = false
        if type == "No abnormal" {
            viewChart.leftAxis.axisMaximum = Double(18)
            viewChart.leftAxis.axisMinimum = Double(14)
        }else{
            viewChart.leftAxis.axisMaximum = Double(valueData.max()!)
            viewChart.leftAxis.axisMinimum = Double(valueData.min()!)
        }
        
        viewChart.scaleYEnabled = false
        viewChart.rightAxis.enabled = false
        viewChart.legend.enabled = false
        viewChart.data = data
        
        return viewChart.asImage()
    }
    
    
    
    func linkImages(arrayImages: [UIImage]) -> UIImage{
        let size = CGSize(width: (arrayImages[0].size.width + 20) * 3, height: arrayImages[0].size.height * 2)
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        
        arrayImages[0].draw(in: CGRect(x: 0, y: 0, width: arrayImages[0].size.width + 10, height: arrayImages[0].size.height))
        arrayImages[1].draw(in: CGRect(x: arrayImages[0].size.width - 10, y: 0, width: arrayImages[1].size.width + 10, height: arrayImages[1].size.height))
        arrayImages[2].draw(in: CGRect(x: (arrayImages[0].size.width * 2) - 20, y: (0), width: arrayImages[2].size.width + 10, height: arrayImages[2].size.height))
        arrayImages[3].draw(in: CGRect(x: 0, y: arrayImages[0].size.height - 10, width: arrayImages[3].size.width + 10, height: arrayImages[3].size.height))
        arrayImages[4].draw(in: CGRect(x: arrayImages[3].size.width - 10, y: arrayImages[4].size.height - 10, width: arrayImages[4].size.width + 10, height: arrayImages[4].size.height))
        arrayImages[5].draw(in: CGRect(x: (arrayImages[5].size.width * 2) - 20, y: arrayImages[0].size.height - 10, width: arrayImages[5].size.width + 10, height: arrayImages[5].size.height))
        
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return newImage
        
    }
    
    func showPDF(fileName: String){
        do{
            let pdfView = PDFView()
            
            pdfView.translatesAutoresizingMaskIntoConstraints = false
            viewPDF.addSubview(pdfView)
            
            pdfView.leadingAnchor.constraint(equalTo: viewPDF.safeAreaLayoutGuide.leadingAnchor).isActive = true
            pdfView.trailingAnchor.constraint(equalTo: viewPDF.safeAreaLayoutGuide.trailingAnchor).isActive = true
            pdfView.topAnchor.constraint(equalTo: viewPDF.safeAreaLayoutGuide.topAnchor).isActive = true
            pdfView.bottomAnchor.constraint(equalTo: viewPDF.safeAreaLayoutGuide.bottomAnchor).isActive = true
            
            if let document = PDFDocument(url: NSURL(fileURLWithPath: fileName, isDirectory: true) as URL) {
                pdfView.document = document
            }
            
        }catch{
            print(error)
        }
    }
}

extension UIImage {
    
    func resize(targetSize: CGSize) -> UIImage {
        return UIGraphicsImageRenderer(size:targetSize).image { _ in
            self.draw(in: CGRect(origin: .zero, size: targetSize))
        }
    }
    
}

extension UIView {
    func asImage() -> UIImage {
        let renderer = UIGraphicsImageRenderer(bounds: bounds)
        return renderer.image { rendererContext in
            layer.render(in: rendererContext.cgContext)
        }
    }
}
