//
//  DataCell.swift
//  SDK_cardiografo
//
//  Created by Camilo Soto on 5/15/19.
//  Copyright © 2019 Pammos. All rights reserved.
//

import UIKit

class DataCell: UITableViewCell {
    
    @IBOutlet weak var txtDate: UILabel!
    @IBOutlet weak var txtHR: UILabel!
    @IBOutlet weak var textTypeMeasure: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func initData(data: CellData){
        txtDate.text = data.date
        txtHR.text = data.valueHR
        textTypeMeasure.text = data.type
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
