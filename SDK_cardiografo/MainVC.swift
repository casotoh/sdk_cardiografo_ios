//
//  ViewController.swift
//  SDK_cardiografo
//
//  Created by Camilo Soto on 5/15/19.
//  Copyright © 2019 Pammos. All rights reserved.
//

import UIKit
import CoreBluetooth
import SimplePDF
import PDFKit

let serviceMainCBUUID = CBUUID(string: "0x1101")

let characteristicConfigCBUUID = CBUUID(string: "49535343-8841-43F4-A8D4-ECBE34729BB3")


let characteristicMeasureCBUUID = CBUUID(string: "0003")
let characteristicECGNotifyCBUUID = CBUUID(string: "3E03")
let characteristicSYSNotifyCBUUID = CBUUID(string: "0003")
let characteristicsPPGNotifyCBUUID = CBUUID(string: "2C03")

let heartRateMeasurementCharacteristicCBUUID = CBUUID(string: "FEA1")
let bodySensorLocationCharacteristicCBUUID = CBUUID(string: "FEC9")
let bateryLevelCharacteristicsCBUUID = CBUUID(string: "2902")

let descriptorCBUUID = CBUUID(string: "")



class MainVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    

    @IBOutlet weak var txtStatus: UILabel!
    @IBOutlet weak var txtMessage: UILabel!
    @IBOutlet weak var btnConnect: UIButton!
    @IBOutlet weak var btnDisconnect: UIButton!
    @IBOutlet weak var btnSync: UIButton!
    @IBOutlet weak var tableData: UITableView!
    
    var centralManager: CBCentralManager!
    var wearPeripheral: CBPeripheral!
    var characteristicConfig: CBCharacteristic!
    var dataActual: CellData?
    var listData: [CellData] = []
    var dataSelect: CellData?
    var dataArray: [UInt8] = []
    var countData = 1
    var sizeData = 0
    var percentege = 0
    var idMeasure: Int64 = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        listData = Queries.getMeasures()
        
        self.tableData.delegate = self
        self.tableData.dataSource = self
        self.tableData.estimatedRowHeight = 64
        
    }
    
    //MARK: TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dataSelect = listData[indexPath.row]
        let cell = tableData.dequeueReusableCell(withIdentifier: "cellData", for: indexPath) as! DataCell
        cell.initData(data: dataSelect)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        self.dataSelect = listData[indexPath.row]
        performSegue(withIdentifier: "toChart", sender: self)
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toChart"{
            let destination = segue.destination as! ChartVC
            destination.dataCell = self.dataSelect!
            //destination.idMeasure = self.dataSelect?.idMeasure ?? 0
        }
    }
    
    //MARK: Acciones de los botones
    @IBAction func connectBluettoth(_ sender: UIButton) {
        centralManager = CBCentralManager(delegate: self, queue: nil)
    }
    
    @IBAction func disconectBluetooth(_ sender: UIButton) {
        centralManager.cancelPeripheralConnection(wearPeripheral)
    }
    
    
    func setParameters() -> Parameters{
        let params = Parameters(language: 1, antialias: true, filter: true, measureMode: true, sound: true, shield: false, timeChoose: 0)
        return params
    }
    
    func processData(arrayData: [UInt8]){
        
        guard arrayData.count > 1 else {return}
        let valueInit = arrayData[0]
        switch valueInit {
        case 0xF3:
            print("Ajuste de parametros exitoso")
            let byteData = CMDBluetooth.cemGetInfoData(type: 1, index: 0)
            self.wearPeripheral.writeValue(byteData, for: characteristicConfig, type: .withResponse)
            self.percentege = 0
            self.countData = 1
            
        case 0xE0:
            self.sizeData =  Int(arrayData[1])
            print("La cantidad de datos almacenados son: \(sizeData)")
            if self.sizeData > 0 {
                print("Obtener la información del clip de ECG, el número de índice del caso = \(self.countData)")
                let byteData = CMDBluetooth.cemGetInfoData(type: 2, index: self.countData)
                self.wearPeripheral.writeValue(byteData, for: characteristicConfig, type: .withResponse)
            }
            
        case 0xE1:
            self.dataActual = CMDBluetooth.getStructInfo(array: arrayData)
            self.idMeasure = self.dataActual!.idMeasure
            self.dataArray = [UInt8](repeating: 0x00, count: self.dataActual!.sizeData)
            
            let byteDataData = CMDBluetooth.cmdGetData(index: self.countData)
            self.wearPeripheral.writeValue(byteDataData, for: characteristicConfig, type: .withResponse)
            
        case 0xD0:
            
            var dataMod = CMDBluetooth.unPack(data: arrayData)
            
            for index in stride(from: 0, to: 49, by: 2) {
                self.dataArray[self.percentege * 50 + index] = dataMod[index + 10 + 1]
                self.dataArray[self.percentege * 50 + index + 1] = dataMod[index + 10]
            }
            
            self.percentege += 1
            print("Este es el porcentaje de avance \(self.percentege)%")
            if self.percentege >= 100 {
                
                saveData(dataCell: self.dataActual!)
                self.listData.append(self.dataActual!)
                self.tableData.reloadData()
                saveDataMeasure(id: self.idMeasure, array: self.dataArray)
                if self.countData < self.sizeData{
                    self.countData += 1
                    self.percentege = 0
                    
                    print("Obtener la información del clip de ECG, el número de índice del caso = \(self.sizeData)")
                    let byteData = CMDBluetooth.cemGetInfoData(type: 2, index: self.countData)
                    self.wearPeripheral.writeValue(byteData, for: characteristicConfig, type: .withResponse)
                }
                else{
                    let cmdDelete = CMDBluetooth.cmdDeleteData()
                    self.wearPeripheral.writeValue(cmdDelete, for: characteristicConfig, type: .withResponse)
                    print("Este es el valor del CounData: \(self.countData)")
                    
                }
            }else{
                print("No existen mas datos almacenados")
            }
        default:
            print("Sin accion: \(valueInit)")
            break
        }
    }
    
    func saveData(dataCell: CellData){
        let dataMeasure = MeasuresDB(identifier: dataCell.idMeasure, type: dataCell.type, date: dataCell.date, hr: Int(dataCell.valueHR), sizeData: dataCell.sizeData)
        Queries.inserMeasure(measure: dataMeasure)
    }
    
    func saveDataMeasure(id: Int64, array: [UInt8]){
        //Convert [UInt8] to NSData
        let data = Data(bytes: array)
        
        //Encode to base64
        let base64Data = data.base64EncodedString(options: .endLineWithLineFeed)
        let dataDb = DataDB(idMeasure: id, data: base64Data)
        Queries.insertDataMeasure(data: dataDb)
        
    }
    
    
}

extension String {
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }
        return String(data: data, encoding: .utf8)
    }
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
}

extension MainVC: CBCentralManagerDelegate {
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .unknown:
            print("central.state is .unknown")
        case .resetting:
            print("central.state is .resetting")
        case .unsupported:
            print("central.state is .unsupported")
        case .unauthorized:
            print("central.state is .unauthorized")
        case .poweredOff:
            print("central.state is .poweredOff")
            txtStatus.text = "Bluetooth Apagado"
            txtStatus.textColor = UIColor(red: 255/255, green: 38/255, blue: 0/255, alpha: 1.0)
        case .poweredOn:
            print("central.state is .poweredOn")
            //centralManager.scanForPeripherals(withServices: [serviceMainCBUUID])
            
            centralManager.scanForPeripherals(withServices: nil)
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral,
                        advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        let device = (advertisementData as NSDictionary)
            .object(forKey: CBAdvertisementDataLocalNameKey) as? NSString
        
        
        
        if device != nil {
            print(peripheral.name!)
            //if peripheral.name! == "CK18S_C871"{
            if peripheral.name!.contains("PM10"){
                print(peripheral)
                txtMessage.text = "Dispositivo conectado: \(peripheral.name!)"
                wearPeripheral = peripheral
                self.wearPeripheral.delegate = self
                centralManager.connect(wearPeripheral, options: nil)
                centralManager.stopScan()
            }
        }
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("Connected!")
        peripheral.delegate = self
        txtStatus.text = "Dispositivo Conectado"
        txtStatus.textColor = UIColor(red: 26/255, green: 189/255, blue: 56/255, alpha: 1.0)
        
        wearPeripheral.discoverServices(nil)
        //wearPeripheral.discoverServices([serviceMainCBUUID])
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        txtStatus.text = "Dispositivo Desconectado"
        txtStatus.textColor = UIColor(red: 255/255, green: 38/255, blue: 0/255, alpha: 1.0)
        txtMessage.text = error?.localizedDescription
    }
}

extension MainVC: CBPeripheralDelegate {
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        guard let services = peripheral.services else { return }
        for service in services {
            print(service)
            //peripheral.discoverCharacteristics([characteristicConfig], for: service)
            peripheral.discoverCharacteristics(nil, for: service)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        guard let characteristics = service.characteristics else { return }
        
        for characteristic in characteristics {
            
            if characteristic.uuid == characteristicConfigCBUUID {
                characteristicConfig = characteristic
                let byteDate = CMDBluetooth.cmdSyncTime()
                self.wearPeripheral.writeValue(byteDate, for: characteristicConfig, type: .withResponse)
                let byteParameters = CMDBluetooth.cmdSetParameters(parameters: setParameters())
                self.wearPeripheral.writeValue(byteParameters, for: characteristicConfig, type: .withResponse)
            }
            
            
            if characteristic.properties.contains(.read) {
                print("\(characteristic.uuid): properties contains .read")
                peripheral.readValue(for: characteristic)
                
            }
            if characteristic.properties.contains(.notify) {
                print("\(characteristic.uuid): properties contains .notify")
                peripheral.setNotifyValue(true, for: characteristic)
            }
            if characteristic.properties.contains(.write) {
                print("\(characteristic.uuid): properties contains .write")
            }
            if characteristic.properties.contains(.writeWithoutResponse) {
                print("\(characteristic.uuid): properties contains .writeWithout")
            }
            print("\(characteristic)\n")
        }
    }
    
    func peripheralManager(peripheral: CBPeripheralManager, didReceiveWriteRequests requests: [CBATTRequest]) {
        
        print("Recepcion de requerimientos")
    }
    
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        print("Write!!! \(characteristic.uuid.uuidString)")
        guard let data = characteristic.value else { return }
        print("\nValue: \(data.base64EncodedString()) \nwas written to Characteristic:\n\(characteristic)")
        if(error != nil){
            print("\nError while writing on Characteristic:\n\(characteristic). Error Message:")
            print(error as Any)
        }
    }
    
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        //print("Update Value!!!\n\n")
        print(characteristic.value! as NSData)
        guard let value = characteristic.value else{return}
        let valueArray = [UInt8](value)
        processData(arrayData: valueArray)        
    }
}

