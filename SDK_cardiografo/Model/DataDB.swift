//
//  DataDB.swift
//  SDK_cardiografo
//
//  Created by Camilo Soto on 5/21/19.
//  Copyright © 2019 Pammos. All rights reserved.
//

import Foundation
import GRDB

class DataDB: Record{
    
    var id: Int64!
    var idMeasure: Int64?
    var data : String?
    
    
    init(idMeasure: Int64? = nil, data : String? = nil){
        self.idMeasure = idMeasure
        self.data = data
        super.init()
    }
    
    //MARK: - Record
    override class var databaseTableName: String {
        return DataDao.NAME_TABLE
    }
    
    
    required init(row: Row) {
        idMeasure = row[DataDao.ID_MEASURE]
        id = row[DataDao.ID]
        data = row[DataDao.DATA]
        super.init(row: row)
    }
    
    override func encode(to container: inout PersistenceContainer) {
        container[DataDao.ID_MEASURE] = idMeasure
        container[DataDao.ID] = id
        container[DataDao.DATA] = data
    }
    
    
    static func fetchAll(db: Database) -> [DataDB] {
        return try! DataDB.fetchAll(db, DataDao.getSelect())
    }
    
    static func fetchAllWithQuery(_ db: Database, _ query: String) -> [DataDB] {
        return try! DataDB.fetchAll(db, DataDao.getSelectWithQuery(query: query))
    }
    
    static func fetchAllWithOtherQuery(db: Database, query: String) -> [DataDB]{
        return try! DataDB.fetchAll(db, DataDao.getSelectWithQueryNotWhere(query: query))
    }
    
    static func fetchOneWithQuery(_ db: Database, _ query : String) -> DataDB? {
        return try! DataDB.fetchOne(db, DataDao.getSelectWithQuery(query: query))
    }
    
    static func deleteRow(_ db: Database, _ id: Int64) -> DataDB?{
        return try! DataDB.fetchOne(db, DataDao.deleteRow(id: id))
    }
}


