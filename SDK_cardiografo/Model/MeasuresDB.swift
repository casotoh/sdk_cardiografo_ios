//
//  MeasuresDB.swift
//  SDK_cardiografo
//
//  Created by Camilo Soto on 5/21/19.
//  Copyright © 2019 Pammos. All rights reserved.
//

import Foundation
import GRDB

class MeasuresDB: Record{
    
    var id: Int64!
    var identifier: Int64?
    var type : String?
    var date: String?
    var hr: Int?
    var sizeData: Int?
    
    
    init(identifier: Int64? = nil, type : String? = nil, date : String? = nil, hr : Int? = nil, sizeData : Int? = nil){
        self.identifier = identifier
        self.type = type
        self.date = date
        self.hr = hr
        self.sizeData = sizeData
        super.init()
    }
    
    //MARK: - Record
    override class var databaseTableName: String {
        return MeasuresDao.NAME_TABLE
    }
    
    
    required init(row: Row) {
        identifier = row[MeasuresDao.IDENTIFIER]
        id = row[MeasuresDao.ID]
        type = row[MeasuresDao.TYPE]
        date = row[MeasuresDao.DATE]
        hr = row[MeasuresDao.HR]
        sizeData = row[MeasuresDao.SIZE_DATA]
        super.init(row: row)
    }
    
    override func encode(to container: inout PersistenceContainer) {
        container[MeasuresDao.IDENTIFIER] = identifier
        container[MeasuresDao.ID] = id
        container[MeasuresDao.TYPE] = type
        container[MeasuresDao.DATE] = date
        container[MeasuresDao.HR] = hr
        container[MeasuresDao.SIZE_DATA] = sizeData
    }
    
    
    static func fetchAll(db: Database) -> [MeasuresDB] {
        return try! MeasuresDB.fetchAll(db, MeasuresDao.getSelect())
    }
    
    static func fetchAllWithQuery(_ db: Database, _ query: String) -> [MeasuresDB] {
        return try! MeasuresDB.fetchAll(db, MeasuresDao.getSelectWithQuery(query: query))
    }
    
    static func fetchAllWithOtherQuery(db: Database, query: String) -> [MeasuresDB]{
        return try! MeasuresDB.fetchAll(db, MeasuresDao.getSelectWithQueryNotWhere(query: query))
    }
    
    static func fetchOneWithQuery(_ db: Database, _ query : String) -> MeasuresDB? {
        return try! MeasuresDB.fetchOne(db, MeasuresDao.getSelectWithQuery(query: query))
    }
    
    static func deleteRow(_ db: Database, _ id: Int64) -> MeasuresDB?{
        return try! MeasuresDB.fetchOne(db, MeasuresDao.deleteRow(id: id))
    }
}


