//
//  MeasuresDao.swift
//  SDK_cardiografo
//
//  Created by Camilo Soto on 5/21/19.
//  Copyright © 2019 Pammos. All rights reserved.
//

import Foundation
import GRDB

class MeasuresDao{
    
    static let NAME_TABLE = "MEASURE"
    static let ID = "id"
    static let IDENTIFIER = "IDENTIFIER"
    static let TYPE = "TYPE"
    static let DATE = "DATE"
    static let HR = "HR"
    static let SIZE_DATA = "SIZE_DATA"
    
    //MARK: - Create Entity
    static func create() throws -> Void {
        var migrator = DatabaseMigrator()
        migrator.registerMigration("v1\(NAME_TABLE)") { db in
            
            try db.create(table: NAME_TABLE){ t in
                t.column(ID, .integer).primaryKey()
                t.column(IDENTIFIER, .integer)
                t.column(TYPE, .text)
                t.column(DATE, .text)
                t.column(HR, .integer)
                t.column(SIZE_DATA, .integer)
            }
        }
        try! migrator.migrate(DBManager.SharedInstance.getDBQueue())
        
    }
    
    
    static func cleanTable() throws -> Void {
        let queue = DBManager.SharedInstance.getDBQueue()
        try! queue.inTransaction { db in
            try db.execute("DELETE FROM \(NAME_TABLE)")
            return .commit
        }
    }
    
    static func getSelect() -> String{
        return "SELECT * FROM \(NAME_TABLE)"
    }
    
    static func getSelectWithQuery(query: String) -> String{
        return "\(getSelect()) WHERE \(query)"
    }
    
    static func getSelectWithQueryNotWhere(query: String) -> String{
        return "\(getSelect()) \(query)"
    }
    
    static func deleteRow(id: Int64) -> String {
        return "DELETE FROM \(NAME_TABLE) WHERE id = \(id)"
    }
}



