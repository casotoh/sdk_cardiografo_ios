//
//  Queries.swift
//  SDK_cardiografo
//
//  Created by Camilo Soto on 5/22/19.
//  Copyright © 2019 Pammos. All rights reserved.
//
import Foundation

let queue = DBManager.SharedInstance.getDBQueue()

class Queries: NSObject {
    
    
    //MARK: Usuarios
    
    class func inserMeasure(measure: MeasuresDB){
        try! queue.inTransaction{ db in
            try! measure.insert(db)
            return .commit
        }
    }
    
    class func getMeasures() -> [CellData]{
        var listData = [CellData]()
        let listMeasures = queue.inDatabase{db in
            MeasuresDB.fetchAll(db: db)
        }
        
        for measure in listMeasures {
            let cellData = CellData(idMeasure: measure.identifier!, date: measure.date!, valueHR: "\(measure.hr!)", type: measure.type!, sizeData: measure.sizeData!)
            listData.append(cellData)
        }
        
        return listData
    }
    
    //MARK: Data Measures
    class func insertDataMeasure(data: DataDB){
        try! queue.inTransaction{ db in
            try! data.insert(db)
            return .commit
        }
    }
    
    class func getDataForIdMeasure(idMeasure: Int64) -> [Float]{
        var array: [Float] = []
        let data = queue.inDatabase{db in
            DataDB.fetchOneWithQuery(db, "\(DataDao.ID_MEASURE) = \(idMeasure)")
        }
        
        if data != nil{
            let arrayData = NSData(base64Encoded: data!.data!, options: .ignoreUnknownCharacters)
            let bytesArray = [UInt8](arrayData! as Data)
            
            array = getArrayUInt16(array: bytesArray)
        }
        
        return array
    }
    
    class func getArrayUInt16(array: [UInt8]) -> [Float]{
        var array16: [Float] = []
        for index in stride(from: 0, to: array.count - 1, by: 2) {
            let uInt16Value = UInt16(array[index]) << 8 | UInt16(array[index + 1])
            let int16Value = Float(Int16(bitPattern: uInt16Value))/1000
            array16.append(int16Value)
        }
        
        var arraySubFinal: [Float] = []
        for index in 1..<array16.count - 1 {
            let x = array16[index]
            let diffA = array16[index - 1] - x
            let diffB = array16[index + 1] - x
            if (diffA > 0.015 && diffB > 0.015) || (diffA < -0.15 && diffB < -0.15){
            }else{
                arraySubFinal.append(x)
            }
        }
        var arrayFinal: [Float] =  []
        for index in 1..<arraySubFinal.count - 1 {
            let x = arraySubFinal[index]
            let diffA = arraySubFinal[index - 1] - x
            let diffB = arraySubFinal[index + 1] - x
            if ((diffA < -0.009 && diffB < -0.009) || (diffA < 0.009 && diffB > -0.01)) && x > 16.7{
            }else{
                arrayFinal.append(x)
            }
        }
        
        return arrayFinal
    }
}


