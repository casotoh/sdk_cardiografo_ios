//
//  Structures.swift
//  SDK_cardiografo
//
//  Created by Camilo Soto on 5/17/19.
//  Copyright © 2019 Pammos. All rights reserved.
//

import Foundation


struct CellData {
    var idMeasure: Int64
    var date: String
    var valueHR: String
    var type: String
    var sizeData: Int
}

struct Parameters {
    var language : Int
    var antialias: Bool
    var filter: Bool
    var measureMode: Bool
    var sound: Bool
    var shield: Bool
    var timeChoose: Int
}
