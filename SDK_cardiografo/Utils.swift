//
//  Utils.swift
//  SDK_cardiografo
//
//  Created by Camilo Soto on 5/30/19.
//  Copyright © 2019 Pammos. All rights reserved.
//

import Foundation
import UIKit
import PDFKit
import SimplePDF

class Utils: NSObject {
    
    class func generatePDF(image: UIImage, dataInfo: CellData) -> String{
        
        let arrayInfo = [["Nombre: Alberto Piedrahita Lopez", "Edad: 30", "Sexo: Masculino"], ["Identificacion: 1020203033", "Peso: 65 Kg", "Hora: \(dataInfo.date)"]]
        
        let fontRegular = UIFont(name: "ArialMT", size: 22)!
        let colorBlack = UIColor.black
        var documentFileName =  ""
        let a4PaperSize = CGSize(width: 1160, height: 1750)
        let pdf = SimplePDF(pageSize: a4PaperSize)
        
        pdf.addLineSpace(20.0)
        pdf.setContentAlignment(.center)
        pdf.addText("Gráfico ECG", font: UIFont(name: "Palatino-Bold", size: 50)!, textColor: UIColor.black)
        pdf.addLineSpace(20.0)
        pdf.addLineSeparator()
        pdf .addTable(2, columnCount: 3, rowHeight: 40, tableLineWidth: 0, tableDefinition: .init(alignments: [.left], columnWidths: [500, 200, 400], fonts: [fontRegular, fontRegular, fontRegular], textColors: [colorBlack]), dataArray: arrayInfo)
        pdf.addLineSeparator()
        
        pdf.setContentAlignment(.center)
        pdf.addLineSpace(20.0)
        pdf.addText("Tiempo: 0 - 10 seg  Escala 1/mV  Frecuencia cardiaca: \(dataInfo.valueHR) bpm", font: UIFont(name: "ArialMT", size: 35)!, textColor: UIColor.black)
        pdf.addLineSpace(10.0)
        print("Tamaño de la imagen \(image.size.width) \(image.size.height)")
        pdf.setContentAlignment(.center)
        pdf.addImage(image)
        
        pdf.addLineSpace(30)
        pdf.addLineSeparator()
        pdf.setContentAlignment(.left)
        
        pdf.addLineSpace(20.0)
        pdf.addText("Resultado:", font: UIFont(name: "Arial-BoldMT", size: 40)!, textColor: UIColor.black)
        pdf.addLineSpace(20.0)
        pdf.addText("             \(dataInfo.type)", font: UIFont(name: "ArialMT", size: 30)!, textColor: UIColor.black)
        pdf.addLineSpace(30.0)
         pdf.setContentAlignment(.right)
        pdf.addText("MAGENTRACK Copyrigth © 2019", font: UIFont(name: "Arial-ItalicMT", size: 20)!, textColor: UIColor.black)
        pdf.addLineSpace(20.0)
        pdf.addLineSeparator()
        pdf.addLineSpace(10.0)
        
        /*pdf.beginNewPage()
         pdf.addText("Begin new page")*/
        
        // Generate PDF data and save to a local file.
        if let documentDirectories = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
            
            let fileName = "report_ecg.pdf"
            documentFileName = documentDirectories + "/" + fileName
            
            let pdfData = pdf.generatePDFdata()
            
            try! pdfData.write(to: NSURL(fileURLWithPath: documentFileName, isDirectory: true) as URL, options: .atomicWrite)
            //try pdfData.writeToFile(documentsFileName, options: .DataWritingAtomic)
            print("\nThe generated pdf can be found at:")
            print("\n\t\(documentFileName)\n")
            
        }
        
        return documentFileName
    }
    
    class func RBResizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return newImage
    }
}
